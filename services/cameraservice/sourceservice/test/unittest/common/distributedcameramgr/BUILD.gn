# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import(
    "//foundation/distributedhardware/distributed_camera/distributedcamera.gni")

module_out_path = "distributed_camera/dcamera_source_mgr_test"

config("module_private_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "${services_path}/cameraservice/sourceservice/include/distributedcamera",
    "${services_path}/cameraservice/sourceservice/include/distributedcameramgr",
    "${services_path}/cameraservice/sourceservice/include/distributedcameramgr/dcameracontrol",
    "${services_path}/cameraservice/sourceservice/include/distributedcameramgr/dcameradata",
    "${services_path}/cameraservice/sourceservice/include/distributedcameramgr/dcamerahdf",
    "${services_path}/cameraservice/sourceservice/include/distributedcameramgr/dcamerainterface",
    "${services_path}/cameraservice/sourceservice/include/distributedcameramgr/dcamerastate",
    "${services_path}/cameraservice/cameraoperator/client/include",
    "${services_path}/cameraservice/base/include",
    "${services_path}/channel/include",
    "${services_path}/data_process/include/pipeline",
    "${services_path}/data_process/include/interfaces",
    "${services_path}/data_process/include/utils",
    "${services_path}/data_process/include/eventbus",
    "${common_path}/include/constants",
    "${common_path}/include/utils",

    "${innerkits_path}/native_cpp/camera_source/include",
    "${innerkits_path}/native_cpp/camera_source/include/callback",
    "${innerkits_path}/native_cpp/camera_sink/include",

    "${fwk_utils_path}/include",
    "${fwk_utils_path}/include/eventbus",
    "${fwk_common_path}/utils/include",
    "//third_party/jsoncpp/include",
    "//drivers/peripheral/base/",
    "${graphicstandard_path}/frameworks/surface/include",
    "${feeding_smoother_path}",
    "${feeding_smoother_path}/base",
    "${feeding_smoother_path}/derived",
    "${feeding_smoother_path}/utils",
  ]
}

ohos_unittest("DCameraSourceMgrTest") {
  module_out_path = module_out_path

  sources = [
    "dcamera_feeding_smoother_test.cpp",
    "dcamera_hdf_operate_test.cpp",
    "dcamera_provider_callback_impl_test.cpp",
    "dcamera_source_config_stream_state_test.cpp",
    "dcamera_source_controller_test.cpp",
    "dcamera_source_data_process_test.cpp",
    "dcamera_source_dev_test.cpp",
    "dcamera_source_input_test.cpp",
    "dcamera_source_service_ipc_test.cpp",
    "dcamera_source_state_machine_test.cpp",
    "dcamera_stream_data_process_producer_test.cpp",
    "dcamera_stream_data_process_test.cpp",
  ]

  configs = [ ":module_private_config" ]

  deps = [
    "${common_path}:distributed_camera_utils",
    "${graphicstandard_path}:libsurface",
    "${services_path}/cameraservice/sourceservice:distributed_camera_source",
    "${services_path}/channel:distributed_camera_channel",
    "${services_path}/data_process:distributed_camera_data_process",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
  ]

  external_deps = [
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "c_utils:utils",
    "drivers_interface_distributed_camera:libdistributed_camera_provider_proxy_1.0",
    "dsoftbus:softbus_client",
    "eventhandler:libeventhandler",
    "hdf_core:libhdi",
    "hilog:libhilog",
    "ipc:ipc_core",
    "samgr:samgr_proxy",
  ]

  defines = [
    "HI_LOG_ENABLE",
    "DH_LOG_TAG=\"DCameraSourceMgrTest\"",
    "LOG_DOMAIN=0xD004100",
  ]
}

group("dcamera_source_mgr_test") {
  testonly = true
  deps = [ ":DCameraSourceMgrTest" ]
}
